'''
La interpolación consiste en encontrar el valor 
de la función F(x), de a cual sólo se conocen 
algunos puntos, para un valor de x que se encuentre 
entre dos valores consecutivos conocidos.

La formula de interpolación más usada es la 
llamada interpolación de Lagrange.
'''

def lagrange(x: list, fx: list , xFind: int) -> float:
    if len(x) != len(fx) :
        raise ValueError("x and fx must have the same lenght")
    
    sizex = len(x)
    product = 1
    Pn = 0
    for i in range(sizex):
        approach1 = 1
        approach2 = 1
        for j in range(sizex): 
            if (j != i):
                approach1 *= (xFind - x[j])
                approach2 *= (x[i] - x[j])
        product = (approach1 / approach2) * fx[i]
        Pn += product
    
    return Pn