def compositeSimpson1_3(h: float, fx:list) -> float:
    n = len(fx) - 1
    summation = fx[0] + fx[n]
    summation_4 = 4 * sum([fx[i] for i in range(1, n, 2)])
    summation_2 = 2 * sum([fx[i] for i in range(2, n - 1, 2)])

    return (h / 3.0) * (summation + summation_4 + summation_2)