'''
Metodo congruencial multiplicativo


    xn = axn-1 % m
donde:
    xn: numero pseudoaleatorios que se genera 
    a: constante numerica seleccionada al azar
    xn-1: al comienzo sera un valor semilla
    m: numero primo lo suficientemente grande
'''

def pseudorandomNumbersFunction(a, m, x0, loops):
    if m == 0:
        raise ZeroDivisionError("m cannot be 0")
    
    randoms = []
    for i in range(loops):
        nextX = (a * x0)  % m
        n = nextX * 1.0 / m * 1.0
        x0 = nextX
        randoms.append(n)

    return randoms