def compositeSimpson3_8(h: float, fx: list) -> float:
    n = len(fx) - 1
    totalSummation = fx[0] + fx[n]
    summation3_1 = summation(fx, 1, n - 1, 3, 3)
    summation3_2 = summation(fx, 2, n, 3, 3)
    summation2 = summation(fx, 3, n - 2, 3, 2)
    totalSummation += summation3_1 + summation3_2 + summation2
    return (3.0 / 8.0) * h * totalSummation


def summation(fx: list, start: int, stop: int, inc: int, mul: int) -> float:
    return mul * sum([fx[i] for i in range(start, stop, inc)])   
