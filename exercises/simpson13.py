'''
La regla de Simpson tiene mayor aproximación 
que la regla del trapecio. 
En la regla del trapecio los puntos sucesivos 
de la grafica y = f(x) se unen mediante líneas 
que forman un trapecio, mientras que en la regla
de Simpson los puntos se unen mediante segmentos
de parábolas.
'''

def simpson1_3(h: int, fx: list):
    return (h / 3) * (fx[0] + (4 * fx[1]) + fx[2])