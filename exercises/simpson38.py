def simpson3_8(h: int, fx: list):
    if len(fx) != 4:
        raise ValueError("fx must contain 4 elements")
    
    return (3.0 / 8.0) * h * (fx[0] + 3 * fx[1] + 3 * fx[2] + fx[3])


