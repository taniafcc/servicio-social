'''
La regla del trapecio es un método de 
integración numérica, es decir, un método 
para calcular aproximadamente el valor de
una integral definida.

La regla se basa en aproximar el valor de 
la integral de f(x) por el de la función 
lineal que pasa a través de los puntos 
(x, f(x)) y (x1, f(x1)). La integral de ésta 
es igual al área del trapecio bajo la gráfica 
de la función lineal.
'''

def trapezoidalRule(x0: int, x1: int, fx0: float, fx1: float) -> float:
    return (x1 - x0) * ((fx1 + fx0) / 2)